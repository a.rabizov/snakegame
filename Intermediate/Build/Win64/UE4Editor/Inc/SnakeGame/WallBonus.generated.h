// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_WallBonus_generated_h
#error "WallBonus.generated.h already included, missing '#pragma once' in WallBonus.h"
#endif
#define SNAKEGAME_WallBonus_generated_h

#define SnakeGame_Source_SnakeGame_WallBonus_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleFoodVisability) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleFoodVisability(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleFoodVisability) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleFoodVisability(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWallBonus(); \
	friend struct Z_Construct_UClass_AWallBonus_Statics; \
public: \
	DECLARE_CLASS(AWallBonus, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AWallBonus) \
	virtual UObject* _getUObject() const override { return const_cast<AWallBonus*>(this); }


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAWallBonus(); \
	friend struct Z_Construct_UClass_AWallBonus_Statics; \
public: \
	DECLARE_CLASS(AWallBonus, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AWallBonus) \
	virtual UObject* _getUObject() const override { return const_cast<AWallBonus*>(this); }


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWallBonus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWallBonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallBonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallBonus(AWallBonus&&); \
	NO_API AWallBonus(const AWallBonus&); \
public:


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallBonus(AWallBonus&&); \
	NO_API AWallBonus(const AWallBonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallBonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWallBonus)


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_WallBonus_h_12_PROLOG
#define SnakeGame_Source_SnakeGame_WallBonus_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_INCLASS \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_WallBonus_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_WallBonus_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AWallBonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_WallBonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
