// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SmallWall_generated_h
#error "SmallWall.generated.h already included, missing '#pragma once' in SmallWall.h"
#endif
#define SNAKEGAME_SmallWall_generated_h

#define SnakeGame_Source_SnakeGame_SmallWall_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleFoodVisability) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleFoodVisability(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleFoodVisability) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleFoodVisability(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASmallWall(); \
	friend struct Z_Construct_UClass_ASmallWall_Statics; \
public: \
	DECLARE_CLASS(ASmallWall, AWall, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASmallWall)


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_INCLASS \
private: \
	static void StaticRegisterNativesASmallWall(); \
	friend struct Z_Construct_UClass_ASmallWall_Statics; \
public: \
	DECLARE_CLASS(ASmallWall, AWall, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASmallWall)


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASmallWall(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASmallWall) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASmallWall); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASmallWall); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASmallWall(ASmallWall&&); \
	NO_API ASmallWall(const ASmallWall&); \
public:


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASmallWall(ASmallWall&&); \
	NO_API ASmallWall(const ASmallWall&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASmallWall); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASmallWall); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASmallWall)


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_SmallWall_h_15_PROLOG
#define SnakeGame_Source_SnakeGame_SmallWall_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_INCLASS \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_SmallWall_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SmallWall_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASmallWall>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_SmallWall_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
