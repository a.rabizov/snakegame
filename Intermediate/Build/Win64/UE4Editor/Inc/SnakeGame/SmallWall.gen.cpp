// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/SmallWall.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSmallWall() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ASmallWall_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASmallWall();
	SNAKEGAME_API UClass* Z_Construct_UClass_AWall();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_ASmallWall_ToggleFoodVisability();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
// End Cross Module References
	void ASmallWall::StaticRegisterNativesASmallWall()
	{
		UClass* Class = ASmallWall::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ToggleFoodVisability", &ASmallWall::execToggleFoodVisability },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASmallWall_ToggleFoodVisability_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASmallWall_ToggleFoodVisability_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SmallWall.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASmallWall_ToggleFoodVisability_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASmallWall, nullptr, "ToggleFoodVisability", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASmallWall_ToggleFoodVisability_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ASmallWall_ToggleFoodVisability_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASmallWall_ToggleFoodVisability()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASmallWall_ToggleFoodVisability_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASmallWall_NoRegister()
	{
		return ASmallWall::StaticClass();
	}
	struct Z_Construct_UClass_ASmallWall_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentToggleNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentToggleNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxToggleNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxToggleNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimerHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TimerHandler;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASmallWall_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWall,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASmallWall_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASmallWall_ToggleFoodVisability, "ToggleFoodVisability" }, // 2869472628
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASmallWall_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SmallWall.h" },
		{ "ModuleRelativePath", "SmallWall.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASmallWall_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "SmallWall" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "SmallWall.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASmallWall_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASmallWall, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASmallWall_Statics::NewProp_MeshComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASmallWall_Statics::NewProp_MeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASmallWall_Statics::NewProp_CurrentToggleNumber_MetaData[] = {
		{ "ModuleRelativePath", "SmallWall.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASmallWall_Statics::NewProp_CurrentToggleNumber = { "CurrentToggleNumber", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASmallWall, CurrentToggleNumber), METADATA_PARAMS(Z_Construct_UClass_ASmallWall_Statics::NewProp_CurrentToggleNumber_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASmallWall_Statics::NewProp_CurrentToggleNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASmallWall_Statics::NewProp_MaxToggleNumber_MetaData[] = {
		{ "Category", "SmallWall" },
		{ "ModuleRelativePath", "SmallWall.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASmallWall_Statics::NewProp_MaxToggleNumber = { "MaxToggleNumber", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASmallWall, MaxToggleNumber), METADATA_PARAMS(Z_Construct_UClass_ASmallWall_Statics::NewProp_MaxToggleNumber_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASmallWall_Statics::NewProp_MaxToggleNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASmallWall_Statics::NewProp_TimerHandler_MetaData[] = {
		{ "ModuleRelativePath", "SmallWall.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASmallWall_Statics::NewProp_TimerHandler = { "TimerHandler", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASmallWall, TimerHandler), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_ASmallWall_Statics::NewProp_TimerHandler_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASmallWall_Statics::NewProp_TimerHandler_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASmallWall_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASmallWall_Statics::NewProp_MeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASmallWall_Statics::NewProp_CurrentToggleNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASmallWall_Statics::NewProp_MaxToggleNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASmallWall_Statics::NewProp_TimerHandler,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASmallWall_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASmallWall>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASmallWall_Statics::ClassParams = {
		&ASmallWall::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASmallWall_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_ASmallWall_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ASmallWall_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASmallWall_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASmallWall()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASmallWall_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASmallWall, 501032854);
	template<> SNAKEGAME_API UClass* StaticClass<ASmallWall>()
	{
		return ASmallWall::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASmallWall(Z_Construct_UClass_ASmallWall, &ASmallWall::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ASmallWall"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASmallWall);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
