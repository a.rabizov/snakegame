// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/SlowDownBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSlowDownBonus() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ASlowDownBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASlowDownBonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASlowDownBonus::StaticRegisterNativesASlowDownBonus()
	{
		UClass* Class = ASlowDownBonus::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ToggleFoodVisability", &ASlowDownBonus::execToggleFoodVisability },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SlowDownBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASlowDownBonus, nullptr, "ToggleFoodVisability", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASlowDownBonus_NoRegister()
	{
		return ASlowDownBonus::StaticClass();
	}
	struct Z_Construct_UClass_ASlowDownBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimerHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TimerHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentToggleNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentToggleNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxToggleNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxToggleNumber;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASlowDownBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASlowDownBonus_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASlowDownBonus_ToggleFoodVisability, "ToggleFoodVisability" }, // 320784964
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowDownBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SlowDownBonus.h" },
		{ "ModuleRelativePath", "SlowDownBonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "SlowDownBonus" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "SlowDownBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASlowDownBonus, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MeshComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_TimerHandler_MetaData[] = {
		{ "ModuleRelativePath", "SlowDownBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_TimerHandler = { "TimerHandler", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASlowDownBonus, TimerHandler), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_TimerHandler_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_TimerHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_CurrentToggleNumber_MetaData[] = {
		{ "Category", "SlowDownBonus" },
		{ "ModuleRelativePath", "SlowDownBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_CurrentToggleNumber = { "CurrentToggleNumber", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASlowDownBonus, CurrentToggleNumber), METADATA_PARAMS(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_CurrentToggleNumber_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_CurrentToggleNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MaxToggleNumber_MetaData[] = {
		{ "Category", "SlowDownBonus" },
		{ "ModuleRelativePath", "SlowDownBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MaxToggleNumber = { "MaxToggleNumber", nullptr, (EPropertyFlags)0x0010000000010015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASlowDownBonus, MaxToggleNumber), METADATA_PARAMS(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MaxToggleNumber_MetaData, ARRAY_COUNT(Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MaxToggleNumber_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASlowDownBonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_TimerHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_CurrentToggleNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASlowDownBonus_Statics::NewProp_MaxToggleNumber,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASlowDownBonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASlowDownBonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASlowDownBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASlowDownBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASlowDownBonus_Statics::ClassParams = {
		&ASlowDownBonus::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASlowDownBonus_Statics::PropPointers,
		InterfaceParams,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_ASlowDownBonus_Statics::PropPointers),
		ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ASlowDownBonus_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ASlowDownBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASlowDownBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASlowDownBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASlowDownBonus, 506092296);
	template<> SNAKEGAME_API UClass* StaticClass<ASlowDownBonus>()
	{
		return ASlowDownBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASlowDownBonus(Z_Construct_UClass_ASlowDownBonus, &ASlowDownBonus::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ASlowDownBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASlowDownBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
