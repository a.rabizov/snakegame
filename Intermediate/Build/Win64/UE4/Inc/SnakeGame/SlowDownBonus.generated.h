// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SlowDownBonus_generated_h
#error "SlowDownBonus.generated.h already included, missing '#pragma once' in SlowDownBonus.h"
#endif
#define SNAKEGAME_SlowDownBonus_generated_h

#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execToggleFoodVisability) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleFoodVisability(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleFoodVisability) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleFoodVisability(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASlowDownBonus(); \
	friend struct Z_Construct_UClass_ASlowDownBonus_Statics; \
public: \
	DECLARE_CLASS(ASlowDownBonus, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASlowDownBonus) \
	virtual UObject* _getUObject() const override { return const_cast<ASlowDownBonus*>(this); }


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASlowDownBonus(); \
	friend struct Z_Construct_UClass_ASlowDownBonus_Statics; \
public: \
	DECLARE_CLASS(ASlowDownBonus, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASlowDownBonus) \
	virtual UObject* _getUObject() const override { return const_cast<ASlowDownBonus*>(this); }


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASlowDownBonus(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASlowDownBonus) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASlowDownBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASlowDownBonus); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASlowDownBonus(ASlowDownBonus&&); \
	NO_API ASlowDownBonus(const ASlowDownBonus&); \
public:


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASlowDownBonus(ASlowDownBonus&&); \
	NO_API ASlowDownBonus(const ASlowDownBonus&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASlowDownBonus); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASlowDownBonus); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASlowDownBonus)


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_10_PROLOG
#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_INCLASS \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SlowDownBonus_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASlowDownBonus>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_SlowDownBonus_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
