// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/WallBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWallBonus() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallBonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_AWallBonus_ToggleFoodVisability();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AWallBonus::StaticRegisterNativesAWallBonus()
	{
		UClass* Class = AWallBonus::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ToggleFoodVisability", &AWallBonus::execToggleFoodVisability },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWallBonus_ToggleFoodVisability_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWallBonus_ToggleFoodVisability_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WallBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWallBonus_ToggleFoodVisability_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWallBonus, nullptr, "ToggleFoodVisability", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWallBonus_ToggleFoodVisability_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWallBonus_ToggleFoodVisability_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWallBonus_ToggleFoodVisability()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWallBonus_ToggleFoodVisability_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWallBonus_NoRegister()
	{
		return AWallBonus::StaticClass();
	}
	struct Z_Construct_UClass_AWallBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimerHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TimerHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentToggleNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentToggleNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxToggleNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxToggleNumber;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWallBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWallBonus_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWallBonus_ToggleFoodVisability, "ToggleFoodVisability" }, // 442031575
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WallBonus.h" },
		{ "ModuleRelativePath", "WallBonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBonus_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "WallBonus" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WallBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWallBonus_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWallBonus, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWallBonus_Statics::NewProp_MeshComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWallBonus_Statics::NewProp_MeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBonus_Statics::NewProp_TimerHandler_MetaData[] = {
		{ "ModuleRelativePath", "WallBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWallBonus_Statics::NewProp_TimerHandler = { "TimerHandler", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWallBonus, TimerHandler), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_AWallBonus_Statics::NewProp_TimerHandler_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWallBonus_Statics::NewProp_TimerHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBonus_Statics::NewProp_CurrentToggleNumber_MetaData[] = {
		{ "Category", "WallBonus" },
		{ "ModuleRelativePath", "WallBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AWallBonus_Statics::NewProp_CurrentToggleNumber = { "CurrentToggleNumber", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWallBonus, CurrentToggleNumber), METADATA_PARAMS(Z_Construct_UClass_AWallBonus_Statics::NewProp_CurrentToggleNumber_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWallBonus_Statics::NewProp_CurrentToggleNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBonus_Statics::NewProp_MaxToggleNumber_MetaData[] = {
		{ "Category", "WallBonus" },
		{ "ModuleRelativePath", "WallBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AWallBonus_Statics::NewProp_MaxToggleNumber = { "MaxToggleNumber", nullptr, (EPropertyFlags)0x0010000000010015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWallBonus, MaxToggleNumber), METADATA_PARAMS(Z_Construct_UClass_AWallBonus_Statics::NewProp_MaxToggleNumber_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWallBonus_Statics::NewProp_MaxToggleNumber_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWallBonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWallBonus_Statics::NewProp_MeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWallBonus_Statics::NewProp_TimerHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWallBonus_Statics::NewProp_CurrentToggleNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWallBonus_Statics::NewProp_MaxToggleNumber,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWallBonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AWallBonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWallBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWallBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWallBonus_Statics::ClassParams = {
		&AWallBonus::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWallBonus_Statics::PropPointers,
		InterfaceParams,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AWallBonus_Statics::PropPointers),
		ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AWallBonus_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AWallBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWallBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWallBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWallBonus, 2758537078);
	template<> SNAKEGAME_API UClass* StaticClass<AWallBonus>()
	{
		return AWallBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWallBonus(Z_Construct_UClass_AWallBonus, &AWallBonus::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AWallBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWallBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
