// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Arena.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArena() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AArena_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AArena();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_AArena_IsImpossibleToSpawn();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_AArena_SetSnakeActor();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeBase_NoRegister();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_AArena_SpawnEatableActor();
	SNAKEGAME_API UFunction* Z_Construct_UFunction_AArena_SpawnSmallWall();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASmallWall_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASlowDownBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASpeedBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodBonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFood_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void AArena::StaticRegisterNativesAArena()
	{
		UClass* Class = AArena::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsImpossibleToSpawn", &AArena::execIsImpossibleToSpawn },
			{ "SetSnakeActor", &AArena::execSetSnakeActor },
			{ "SpawnEatableActor", &AArena::execSpawnEatableActor },
			{ "SpawnSmallWall", &AArena::execSpawnSmallWall },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics
	{
		struct Arena_eventIsImpossibleToSpawn_Parms
		{
			FVector NewSpawnLocation;
			int32 MinPossibleDistance;
			int32 MaxPossibleDistance;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxPossibleDistance;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MinPossibleDistance;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NewSpawnLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Arena_eventIsImpossibleToSpawn_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Arena_eventIsImpossibleToSpawn_Parms), &Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_MaxPossibleDistance = { "MaxPossibleDistance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Arena_eventIsImpossibleToSpawn_Parms, MaxPossibleDistance), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_MinPossibleDistance = { "MinPossibleDistance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Arena_eventIsImpossibleToSpawn_Parms, MinPossibleDistance), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_NewSpawnLocation = { "NewSpawnLocation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Arena_eventIsImpossibleToSpawn_Parms, NewSpawnLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_MaxPossibleDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_MinPossibleDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::NewProp_NewSpawnLocation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArena, nullptr, "IsImpossibleToSpawn", sizeof(Arena_eventIsImpossibleToSpawn_Parms), Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArena_IsImpossibleToSpawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArena_IsImpossibleToSpawn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArena_SetSnakeActor_Statics
	{
		struct Arena_eventSetSnakeActor_Parms
		{
			ASnakeBase* SnakeActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnakeActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AArena_SetSnakeActor_Statics::NewProp_SnakeActor = { "SnakeActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Arena_eventSetSnakeActor_Parms, SnakeActor), Z_Construct_UClass_ASnakeBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArena_SetSnakeActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArena_SetSnakeActor_Statics::NewProp_SnakeActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArena_SetSnakeActor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArena_SetSnakeActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArena, nullptr, "SetSnakeActor", sizeof(Arena_eventSetSnakeActor_Parms), Z_Construct_UFunction_AArena_SetSnakeActor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AArena_SetSnakeActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArena_SetSnakeActor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AArena_SetSnakeActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArena_SetSnakeActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArena_SetSnakeActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArena_SpawnEatableActor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArena_SpawnEatableActor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArena_SpawnEatableActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArena, nullptr, "SpawnEatableActor", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArena_SpawnEatableActor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AArena_SpawnEatableActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArena_SpawnEatableActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArena_SpawnEatableActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArena_SpawnSmallWall_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArena_SpawnSmallWall_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArena_SpawnSmallWall_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArena, nullptr, "SpawnSmallWall", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArena_SpawnSmallWall_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AArena_SpawnSmallWall_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArena_SpawnSmallWall()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArena_SpawnSmallWall_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AArena_NoRegister()
	{
		return AArena::StaticClass();
	}
	struct Z_Construct_UClass_AArena_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmallWallClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SmallWallClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedWallPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnedWallPtr;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Snake_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Snake;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlowDownBonusClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SlowDownBonusClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpeedBonusClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpeedBonusClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WallBonusClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WallBonusClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodBonusClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoodBonusClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoodClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFoodElem_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewFoodElem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Padding_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Padding;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnCoorinate_Z_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpawnCoorinate_Z;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnCoorinate_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpawnCoorinate_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnCoorinate_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpawnCoorinate_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedActorPtr_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnedActorPtr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArena_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AArena_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AArena_IsImpossibleToSpawn, "IsImpossibleToSpawn" }, // 590892014
		{ &Z_Construct_UFunction_AArena_SetSnakeActor, "SetSnakeActor" }, // 1306844198
		{ &Z_Construct_UFunction_AArena_SpawnEatableActor, "SpawnEatableActor" }, // 2526254169
		{ &Z_Construct_UFunction_AArena_SpawnSmallWall, "SpawnSmallWall" }, // 4080179503
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Arena.h" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SmallWallClass_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SmallWallClass = { "SmallWallClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SmallWallClass), Z_Construct_UClass_ASmallWall_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SmallWallClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SmallWallClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SpawnedWallPtr_MetaData[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SpawnedWallPtr = { "SpawnedWallPtr", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SpawnedWallPtr), Z_Construct_UClass_ASmallWall_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SpawnedWallPtr_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SpawnedWallPtr_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_Snake_MetaData[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_Snake = { "Snake", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, Snake), Z_Construct_UClass_ASnakeBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_Snake_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_Snake_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SlowDownBonusClass_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SlowDownBonusClass = { "SlowDownBonusClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SlowDownBonusClass), Z_Construct_UClass_ASlowDownBonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SlowDownBonusClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SlowDownBonusClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SpeedBonusClass_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SpeedBonusClass = { "SpeedBonusClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SpeedBonusClass), Z_Construct_UClass_ASpeedBonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SpeedBonusClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SpeedBonusClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_WallBonusClass_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_WallBonusClass = { "WallBonusClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, WallBonusClass), Z_Construct_UClass_AWallBonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_WallBonusClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_WallBonusClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_FoodBonusClass_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_FoodBonusClass = { "FoodBonusClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, FoodBonusClass), Z_Construct_UClass_AFoodBonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_FoodBonusClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_FoodBonusClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_FoodClass_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_FoodClass = { "FoodClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, FoodClass), Z_Construct_UClass_AFood_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_FoodClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_FoodClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_NewFoodElem_MetaData[] = {
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_NewFoodElem = { "NewFoodElem", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, NewFoodElem), Z_Construct_UClass_AFood_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_NewFoodElem_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_NewFoodElem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_Padding_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_Padding = { "Padding", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, Padding), METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_Padding_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_Padding_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Z_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Z = { "SpawnCoorinate_Z", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SpawnCoorinate_Z), METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Z_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Z_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Y_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Y = { "SpawnCoorinate_Y", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SpawnCoorinate_Y), METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Y_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_X_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_X = { "SpawnCoorinate_X", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SpawnCoorinate_X), METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_X_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArena_Statics::NewProp_SpawnedActorPtr_MetaData[] = {
		{ "Category", "Arena" },
		{ "ModuleRelativePath", "Arena.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArena_Statics::NewProp_SpawnedActorPtr = { "SpawnedActorPtr", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArena, SpawnedActorPtr), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::NewProp_SpawnedActorPtr_MetaData, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::NewProp_SpawnedActorPtr_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AArena_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SmallWallClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SpawnedWallPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_Snake,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SlowDownBonusClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SpeedBonusClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_WallBonusClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_FoodBonusClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_FoodClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_NewFoodElem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_Padding,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Z,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SpawnCoorinate_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArena_Statics::NewProp_SpawnedActorPtr,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArena_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArena>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArena_Statics::ClassParams = {
		&AArena::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AArena_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AArena_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AArena_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArena()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArena_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArena, 3794938780);
	template<> SNAKEGAME_API UClass* StaticClass<AArena>()
	{
		return AArena::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArena(Z_Construct_UClass_AArena, &AArena::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AArena"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArena);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
