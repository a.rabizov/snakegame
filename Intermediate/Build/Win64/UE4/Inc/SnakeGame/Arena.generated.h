// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeBase;
struct FVector;
#ifdef SNAKEGAME_Arena_generated_h
#error "Arena.generated.h already included, missing '#pragma once' in Arena.h"
#endif
#define SNAKEGAME_Arena_generated_h

#define SnakeGame_Source_SnakeGame_Arena_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnSmallWall) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnSmallWall(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetSnakeActor) \
	{ \
		P_GET_OBJECT(ASnakeBase,Z_Param_SnakeActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetSnakeActor(Z_Param_SnakeActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsImpossibleToSpawn) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_NewSpawnLocation); \
		P_GET_PROPERTY(UIntProperty,Z_Param_MinPossibleDistance); \
		P_GET_PROPERTY(UIntProperty,Z_Param_MaxPossibleDistance); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsImpossibleToSpawn(Z_Param_NewSpawnLocation,Z_Param_MinPossibleDistance,Z_Param_MaxPossibleDistance); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnEatableActor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnEatableActor(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_Arena_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnSmallWall) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnSmallWall(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetSnakeActor) \
	{ \
		P_GET_OBJECT(ASnakeBase,Z_Param_SnakeActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetSnakeActor(Z_Param_SnakeActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsImpossibleToSpawn) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_NewSpawnLocation); \
		P_GET_PROPERTY(UIntProperty,Z_Param_MinPossibleDistance); \
		P_GET_PROPERTY(UIntProperty,Z_Param_MaxPossibleDistance); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsImpossibleToSpawn(Z_Param_NewSpawnLocation,Z_Param_MinPossibleDistance,Z_Param_MaxPossibleDistance); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnEatableActor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnEatableActor(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_Arena_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArena(); \
	friend struct Z_Construct_UClass_AArena_Statics; \
public: \
	DECLARE_CLASS(AArena, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AArena)


#define SnakeGame_Source_SnakeGame_Arena_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAArena(); \
	friend struct Z_Construct_UClass_AArena_Statics; \
public: \
	DECLARE_CLASS(AArena, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AArena)


#define SnakeGame_Source_SnakeGame_Arena_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArena(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArena) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArena); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArena); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArena(AArena&&); \
	NO_API AArena(const AArena&); \
public:


#define SnakeGame_Source_SnakeGame_Arena_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArena(AArena&&); \
	NO_API AArena(const AArena&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArena); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArena); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArena)


#define SnakeGame_Source_SnakeGame_Arena_h_20_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_Arena_h_17_PROLOG
#define SnakeGame_Source_SnakeGame_Arena_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Arena_h_20_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Arena_h_20_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_Arena_h_20_INCLASS \
	SnakeGame_Source_SnakeGame_Arena_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_Arena_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Arena_h_20_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Arena_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Arena_h_20_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Arena_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AArena>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_Arena_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
