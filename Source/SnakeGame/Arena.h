// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Arena.generated.h"

class AFood;
class AFoodBonus;
class ASpeedBonus;
class ASlowDownBonus;
class ASnakeBase;
class ASmallWall;
class AWallBonus;

UCLASS()
class SNAKEGAME_API AArena : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArena();

	UPROPERTY(BlueprintReadOnly)
	AActor* SpawnedActorPtr;

	UPROPERTY(EditDefaultsOnly)
	int32 SpawnCoorinate_X;

	UPROPERTY(EditDefaultsOnly)
	int32 SpawnCoorinate_Y;

	UPROPERTY(EditDefaultsOnly)
	int32 SpawnCoorinate_Z;

	UPROPERTY(EditDefaultsOnly)
	int32 Padding;

	UPROPERTY()
	AFood* NewFoodElem;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodBonus> FoodBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallBonus> WallBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpeedBonus> SpeedBonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASlowDownBonus> SlowDownBonusClass;

	UPROPERTY()
	ASnakeBase* Snake;

	UPROPERTY()
	ASmallWall* SpawnedWallPtr;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASmallWall> SmallWallClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawnEatableActor();

	UFUNCTION()
	bool IsImpossibleToSpawn(FVector NewSpawnLocation, int MinPossibleDistance, int MaxPossibleDistance);

	UFUNCTION()
	void SetSnakeActor(ASnakeBase* SnakeActor);

	UFUNCTION()
	void SpawnSmallWall();


};
