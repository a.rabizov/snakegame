// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonus.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASpeedBonus::ASpeedBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MaxToggleNumber = 30;
	CurrentToggleNumber = 0;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ASpeedBonus::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &ASpeedBonus::ToggleFoodVisability, 0.2f, true);
	
}

// Called every frame
void ASpeedBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetMovementSpeed(0.3);
			this->Destroy();
		}
	}
}

void ASpeedBonus::ToggleFoodVisability()
{
	MeshComponent->ToggleVisibility();

	if (CurrentToggleNumber == 2 * MaxToggleNumber)
	{
		this->Destroy();
	}
	CurrentToggleNumber++;

}


