// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBonus.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
AFoodBonus::AFoodBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MaxToggleNumber = 30;
	CurrentToggleNumber = 0;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFoodBonus::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &AFoodBonus::ToggleFoodVisability, 0.2f, true);
}

// Called every frame
void AFoodBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(3);
			this->Destroy();
		}
	}
}

void AFoodBonus::ToggleFoodVisability()
{
	MeshComponent->ToggleVisibility();

	if (CurrentToggleNumber == 2 * MaxToggleNumber)
	{
		this->Destroy();
	}
	CurrentToggleNumber++;

}
