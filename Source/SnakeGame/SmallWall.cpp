// Fill out your copyright notice in the Description page of Project Settings.


#include "SmallWall.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"


ASmallWall::ASmallWall()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MaxToggleNumber = 40;
	CurrentToggleNumber = 0;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

void ASmallWall::ToggleFoodVisability()
{
	MeshComponent->ToggleVisibility();

	if (CurrentToggleNumber == 2 * MaxToggleNumber)
	{
		this->Destroy();
	}
	CurrentToggleNumber++;
}

void ASmallWall::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &ASmallWall::ToggleFoodVisability, 0.6f, true);

}