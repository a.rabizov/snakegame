// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};
UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(BlueprintReadWrite)
	float Health;


	UPROPERTY(BlueprintReadWrite)
	int32 Score;

	UPROPERTY()
	FTimerHandle TimerHandler;
	
	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY()
	EMovementDirection CurrentMoveDirection;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION()
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION()
	void SetMovementSpeed(float NewSpeed);

	UFUNCTION()
	void AddHealth(float value = 20);

	UFUNCTION()
	void AddScore(float value = 1);

	UFUNCTION()
	void ToggleWallCollision();

	UFUNCTION()
	void SetWallCollisionTimer();
};
