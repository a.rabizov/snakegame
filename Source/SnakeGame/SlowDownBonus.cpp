// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowDownBonus.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASlowDownBonus::ASlowDownBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MaxToggleNumber = 30;
	CurrentToggleNumber = 0;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ASlowDownBonus::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &ASlowDownBonus::ToggleFoodVisability, 0.2f, true);
}

// Called every frame
void ASlowDownBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlowDownBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetMovementSpeed(0.7);
			this->Destroy();
		}
	}
}

void ASlowDownBonus::ToggleFoodVisability()
{
	MeshComponent->ToggleVisibility();

	if (CurrentToggleNumber == 2 * MaxToggleNumber)
	{
		this->Destroy();
	}
	CurrentToggleNumber++;

}

