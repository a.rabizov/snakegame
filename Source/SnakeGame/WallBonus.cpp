// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBonus.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AWallBonus::AWallBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MaxToggleNumber = 30;
	CurrentToggleNumber = 0;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AWallBonus::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandler, this, &AWallBonus::ToggleFoodVisability, 0.2f, true);
}

// Called every frame
void AWallBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->ToggleWallCollision();
			Snake->SetWallCollisionTimer();

			this->Destroy();
		}
	}
}

void AWallBonus::ToggleFoodVisability()
{
	MeshComponent->ToggleVisibility();

	if (CurrentToggleNumber == 2 * MaxToggleNumber)
	{
		this->Destroy();
	}
	CurrentToggleNumber++;
}


