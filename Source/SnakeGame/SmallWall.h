// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Wall.h"
#include "SmallWall.generated.h"

/**
 * 
 */

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ASmallWall : public AWall
{
	GENERATED_BODY()
	

public:

	ASmallWall();

	UPROPERTY()
	FTimerHandle TimerHandler;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxToggleNumber;

	UPROPERTY()
	int32 CurrentToggleNumber;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	UFUNCTION()
	void ToggleFoodVisability();
};
